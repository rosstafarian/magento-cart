package com.store.client;

import com.store.cart.domain.Cart;
import com.store.cart.domain.CartImpl;
import com.store.product.domain.Product;
import com.store.product.domain.ProductImpl;
import junit.framework.TestCase;

import java.math.BigDecimal;

public class CheckoutTest extends TestCase {

    public void testCheckoutAmountQuantity() throws Exception {
        Product keyboard = new ProductImpl(1, "Keyboard", new BigDecimal(45.00 ));
        Product mouse = new ProductImpl(2, "Mouse", new BigDecimal(10.00 ));

        Cart myCart = new CartImpl();
        myCart.addProduct(keyboard);
        myCart.addProduct(mouse);

        BigDecimal expectedTotal = new BigDecimal(30);
        BigDecimal actualTotal = myCart.getTotalPrice();

        assertEquals(expectedTotal, actualTotal);
    }

    public void testCheckoutAmount() throws Exception {
        Product keyboard = new ProductImpl(1, "Keyboard", new BigDecimal(55.00 ));

        Cart myCart = new CartImpl();
        myCart.addProduct(keyboard);

        BigDecimal expectedTotal = new BigDecimal(45);
        BigDecimal actualTotal = myCart.getTotalPrice();

        assertEquals(expectedTotal, actualTotal);
    }

    public void testCheckoutQuantity() throws Exception {
        Product keyboard = new ProductImpl(1, "Keyboard", new BigDecimal(15.00 ));
        Product mouse = new ProductImpl(2, "Mouse", new BigDecimal(10.00 ));

        Cart myCart = new CartImpl();
        myCart.addProduct(keyboard);
        myCart.addProduct(mouse);

        BigDecimal expectedTotal = new BigDecimal(10);
        BigDecimal actualTotal = myCart.getTotalPrice();

        assertEquals(expectedTotal, actualTotal);
    }

    public void testCheckoutNull() throws Exception {
        Product keyboard = new ProductImpl(1, "Keyboard", new BigDecimal(15.00 ));

        Cart myCart = new CartImpl();
        myCart.addProduct(keyboard);

        BigDecimal expectedTotal = new BigDecimal(15);
        BigDecimal actualTotal = myCart.getTotalPrice();

        assertEquals(expectedTotal, actualTotal);
    }

    public void testEmptyCart() throws Exception {
        Cart myCart = new CartImpl();

        BigDecimal expectedTotal = new BigDecimal(0);
        BigDecimal actualTotal = myCart.getTotalPrice();

        assertEquals(expectedTotal, actualTotal);
    }

    public void testFullCart() throws Exception {
        Product keyboard = new ProductImpl(1, "Keyboard", new BigDecimal(45.00 ));
        Product mouse = new ProductImpl(2, "Mouse", new BigDecimal(10.00 ));
        Product monitor = new ProductImpl(3, "144hz Ultra-Wide Monitor", new BigDecimal(250.00 ));
        Product ssd = new ProductImpl(4, "Solid State Drive", new BigDecimal(150.00 ));

        Cart myCart = new CartImpl();
        myCart.addProduct(keyboard);
        myCart.addProduct(mouse);
        myCart.addProduct(monitor);
        myCart.addProduct(ssd);

        BigDecimal expectedTotal = new BigDecimal(430);
        BigDecimal actualTotal = myCart.getTotalPrice();

        assertEquals(expectedTotal, actualTotal);
    }

    public void testRemoveItem() throws Exception {
        Product keyboard = new ProductImpl(1, "Keyboard", new BigDecimal(15.00 ));
        Product mouse = new ProductImpl(2, "Mouse", new BigDecimal(10.00 ));
        Product monitor = new ProductImpl(3, "144hz Ultra-Wide Monitor", new BigDecimal(250.00 ));
        Product ssd = new ProductImpl(4, "Solid State Drive", new BigDecimal(150.00 ));

        Cart myCart = new CartImpl();
        myCart.addProduct(keyboard);
        myCart.addProduct(mouse);
        myCart.addProduct(monitor);
        myCart.addProduct(ssd);
        myCart.removeProduct(monitor);
        myCart.removeProduct(ssd);

        BigDecimal expectedTotal = new BigDecimal(10);
        BigDecimal actualTotal = myCart.getTotalPrice();

        assertEquals(expectedTotal, actualTotal);
    }
}
