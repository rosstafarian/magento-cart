package com.store.product.domain;

import java.math.BigDecimal;

public class ProductImpl implements Product {

    private int id;
    private String name;
    private BigDecimal price;

    /**
     * Created a Product
     * @param id Unique identifier for a Product.
     * @param name The Product's name.
     * @param price The Product's price.
     */
    public ProductImpl(int id, String name, BigDecimal price) {
        super();
        this.id = id;
        this.name = name;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    /**
     * For the future.
     * Example: admin functionality to adjust price on a persistent object managed in a DB.
     * @param price The price of the Product to be updated via an authorized method.
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

}
