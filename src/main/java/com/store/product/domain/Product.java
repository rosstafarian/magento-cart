package com.store.product.domain;

import java.math.BigDecimal;

public interface Product {

    public int getId();

    public String getName();

    public BigDecimal getPrice();

}
