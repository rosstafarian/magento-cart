package com.store.discount.domain;

import com.store.config.DiscountConfig;

import java.math.BigDecimal;

public class AmountDiscount implements Discount {

    public BigDecimal applyDiscount(BigDecimal price) {
        return price.subtract(this.getDiscountAmount());
    }

    /**
     * Returns the Discount's amount for the $ amount limit condition.
     * Would normally return value from a DB here.
     */
    public BigDecimal getDiscountAmount() {
        return DiscountConfig.cartAmountDiscountAmount;
    }

    public String getDescription() {
        return "Discount based on $ amount";
    }

}
