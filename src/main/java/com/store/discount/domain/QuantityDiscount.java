package com.store.discount.domain;

import com.store.config.DiscountConfig;

import java.math.BigDecimal;

public class QuantityDiscount implements Discount {

    public BigDecimal applyDiscount(BigDecimal price) {
        return price.subtract(this.getDiscountAmount());
    }

    /**
     * Returns the Discount's amount for the cart item count condition.
     * Would normally return value from a DB here
     */
    public BigDecimal getDiscountAmount() {
        return DiscountConfig.itemCountDiscountAmount;
    }

    public BigDecimal getDiscountRequirement() {
        return DiscountConfig.itemCountDiscountRequirement;
    }

    public String getDescription() {
        return "Discount based on cart item count";
    }

}
