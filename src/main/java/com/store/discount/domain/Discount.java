package com.store.discount.domain;

import java.math.BigDecimal;

public interface Discount {

    public String getDescription();

    public BigDecimal getDiscountAmount();

}
