package com.store.discount.domain;

import com.store.config.DiscountConfig;

import java.math.BigDecimal;

public class AmountQuantityDiscount implements Discount {

    public BigDecimal applyDiscount(BigDecimal price) {
        return price.subtract(this.getDiscountAmount());
    }

    /**
     * Returns the Discount's amount for the $ amount limit and cart item count conditions.
     * Would normally return value from a DB here
     */
    public BigDecimal getDiscountAmount() {
        return DiscountConfig.cartAmountDiscountAmount.add(DiscountConfig.itemCountDiscountAmount);
    }

    public String getDescription() {
        return "Discount based on $ amount and cart item count";
    }

}
