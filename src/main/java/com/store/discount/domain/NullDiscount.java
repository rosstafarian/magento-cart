package com.store.discount.domain;

import java.math.BigDecimal;

public class NullDiscount implements Discount {

    public BigDecimal applyDiscount(BigDecimal price) {
        return price.subtract(this.getDiscountAmount());
    }

    /**
     * Default zero value.
     * Returns the no Discount amount.
     * Would normally return value from a DB here
     */
    public BigDecimal getDiscountAmount() {
        return new BigDecimal(0);
    }

    public String getDescription() {
        return "No discount applied";
    }

}
