package com.store.discount.factory;

import com.store.config.DiscountConfig;
import com.store.discount.domain.*;

import java.math.BigDecimal;

public class DiscountFactory {

    /**
     *
     * @param cartPrice The total cost of all products in the cart before any discount(s).
     * @param cartSize The number of products in the cart.
     * @return The the Discount class to be applied to the cart based upon conditional logic.
     *
     * This factory method would normally pull from a DB having the discounts defined
     * and if they are active or inactive
     */
    public Discount getActiveDiscount(BigDecimal cartPrice, int cartSize) {
        /*
         * Here I would have Hibernate or some other persistence framework pull from a table
         * with the fields id, discount_description, discount_amount, discount_requirement, etc...
         *
         * Once you have those as Discount objects, you can iterate over them and apply dynamically based on defined conditions.
         *
         * But for the sake of brevity, we'll go with this.
         */

        if (cartSize >= DiscountConfig.itemCountDiscountRequirement.intValue() &&
                cartPrice.compareTo(DiscountConfig.cartAmountDiscountRequirement) == 1) {
            return new AmountQuantityDiscount();
        }
        else if (cartSize >= DiscountConfig.itemCountDiscountRequirement.intValue()) {
            return new QuantityDiscount();
        }
        else if (cartPrice.compareTo(DiscountConfig.cartAmountDiscountRequirement) == 1) {
            return new AmountDiscount();
        }

        return new NullDiscount();
    }

}
