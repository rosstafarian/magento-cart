package com.store.config;

import java.math.BigDecimal;

public class DiscountConfig {

    /*
     * These values would normally be pulled from a database or some other value store,
     * but for this it's going to be static.
     */

    public static final BigDecimal itemCountDiscountRequirement = new BigDecimal(2);
    public static final BigDecimal cartAmountDiscountRequirement = new BigDecimal(50);

    public static final BigDecimal itemCountDiscountAmount = new BigDecimal(15);
    public static final BigDecimal cartAmountDiscountAmount = new BigDecimal(10);

}
