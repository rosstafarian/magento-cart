package com.store.client;

import com.store.cart.domain.Cart;
import com.store.cart.domain.CartImpl;
import com.store.product.domain.Product;
import com.store.product.domain.ProductImpl;

import java.math.BigDecimal;

public class Checkout
{
    public static void main( String[] args )
    {
        Product keyboard = new ProductImpl(1, "Keyboard", new BigDecimal(45.00 ));
        Product mouse = new ProductImpl(2, "Mouse", new BigDecimal(10.00 ));

        Cart myCart = new CartImpl();
        myCart.addProduct(keyboard);
        myCart.addProduct(mouse);

        System.out.println("Cart Contents: ");
        for (Product product : myCart.getProductList()) {
            System.out.println(product.getName() + " Original Price: $" + product.getPrice());
        }

        System.out.println("\nDiscount Applied: " + myCart.getDiscount().getDescription());
        System.out.println("-----------------------------------------------------------------");
        System.out.println("Final Price: " + myCart.getTotalPrice());
    }
}
