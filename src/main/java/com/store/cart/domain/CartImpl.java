package com.store.cart.domain;

import com.store.discount.domain.Discount;
import com.store.discount.domain.NullDiscount;
import com.store.discount.factory.DiscountFactory;
import com.store.product.domain.Product;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class CartImpl implements Cart {

    private List<Product> products = new ArrayList<Product>();
    private Discount discount = new NullDiscount();
    private BigDecimal listPrice = new BigDecimal(0);
    private Boolean hasChanged = false;

    /**
     * Gets the total price of the user's Cart. This is with all discounts applied.
     * @return The total price of the items in the cart with all discounts applied.
     */
    public BigDecimal getTotalPrice() {
        BigDecimal totalPrice = this.getListPrice();

        totalPrice = totalPrice.subtract(this.getDiscount().getDiscountAmount());

        return totalPrice;
    }

    /**
     * Gets the discount amount to be applied to the cart total via the correct Discount model.
     * @return The new totalPrice with the discount(s) applied.
     */

    public Discount getDiscount() {
        this.discount = new DiscountFactory().getActiveDiscount(this.getListPrice(), this.getProductList().size());
        return this.discount;
    }

    public List<Product> getProductList() {
        return this.products;
    }

    public void addProduct(Product product) {
        this.products.add(product);
        this.hasChanged = true;
    }

    public void removeProduct(Product product) {
        this.products.remove(product);
        this.hasChanged = true;
    }

    private BigDecimal getListPrice() {
        if (this.hasChanged) {
            for (Product product : this.getProductList()) {
                this.listPrice = this.listPrice.add(product.getPrice());
            }
            this.hasChanged = false;
        }
        return this.listPrice;
    }

}
