package com.store.cart.domain;

import com.store.discount.domain.Discount;
import com.store.product.domain.Product;

import java.math.BigDecimal;
import java.util.List;

public interface Cart {

    public Discount getDiscount();

    public BigDecimal getTotalPrice();

    public List<Product> getProductList();

    public void addProduct(Product product);

    public void removeProduct(Product product);

}
