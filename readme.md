# Magento Cart

### Demo shopping cart with discount logic applied

#### Environment Versions

- Java - JDK 1.8.0_102
- Testing - JUnit 3.8.1

#### SOLID Principles

- **Single Responsibility**: Each class (Product, Cart, Discount) is only responsible for itself and it's behavior is encapsulated.
- **Open-Closed**: Each entity can be extended to add functionality, but is closed for outside modification. For example, a Product can be extended to add non-default functionality, but is only accessed through its interface.
- **Liskov Substitution**: Each Discount entity is accessed through the Discount interfaced and can be substituted for Discount itself. 
- **Interface Segregation**: Each main entity is accessed through an interface and not directly. The interfaces are segregated and do not cross domains.
- **Dependency Inversion**: There is minimized risk in extension due to the object composition. Any of the entities can be expanded without impact to the superclasses. 

#### Design Patterns

- **Strategy**: The Strategy pattern is used to apply various Discount models through the Discount interface when calculating the total price of the Cart.
- **Factory Method**: The Factory Method pattern is used to provide the Cart with the appropriate Discount model to apply to the total cost of the Cart. The logic for determining this is encapsulated within the Factory.
 
#### Possible Extension/Improvement

- **Adding Discounts**: Future Discounts can be added without modifying the Cart/Discount base logic. The only logic to be maintained is kept in the Factory to determine the right Discount model to apply.
- **Database Driven Discounts**: Storing the discount information/requirements in a database could clean up the Factory if/else scenario and allow it to iterate through a collection of discount requirements and apply the correct Discount model to the Cart. This would also eliminate any need for a hardcoded DiscountConfig class/file. 
- **Admin Functionality**: A method was added to ProductImpl but not exposed through the Product interface that allows the Product's price to mutable. This would be controlled through an admin interface or through a batch process but not allowed by an unauthorized client.
